## INSTRUÇÕES PARA O TESTE TÉCNICO
* Crie um fork deste projeto (https://gitlab.com/bancobmg/testetecnico.apivenda/-/forks/new). É preciso estar logado na sua conta Gitlab.
* Adicione @BancoBmg como membro do seu fork (colocar como Maintainer). Você pode fazer isto em  https://gitlab.com/seu-usuario/testetecnico.apivenda/settings/members.
* Procure fazer commits com um conjunto coeso de modificações (ex.: inclusão de uma classe + seu teste).
* Não é necessário criar branches, os commits devem ser feitos diretamente na master do seu fork.
* Ao final, nos informe o seu usuário (https://gitlab.com/seu-usuario)

## O TESTE
* Construir uma API REST utilizando .net core 3.1 ou .net 5.
* A API deve expor uma rota com documentação swagger (http://.../swagger).
* A API deve possuir 3 operações:  
  1) Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento".  
  2) Buscar venda: Busca pelo Id da venda. 
  3) Atualizar venda: Permite que seja atualizado o status da venda
     * OBS.: Possíveis status: “Pagamento aprovado” | “Enviado para transportadora” | “Entregue” | “Cancelada”
* Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos. 
* O vendedor deve possuir id, cpf, nome, e-mail e telefone. 
* A inclusão de uma venda deve possuir pelo menos 1 item. 
* A atualização de status deve permitir somente as seguintes transições: 
  * De: “Aguardando pagamento.” Para: “Pagamento Aprovado“
  * De: “Aguardando pagamento. “ Para: “Cancelada“
  * De: “Pagamento Aprovado. “ Para: “Enviado para Transportadora“
  * De: “Pagamento Aprovado. “ Para: “Cancelada“
  * De: “Enviado para Transportador“. Para: Entregue 
* A API não precisa ter mecanismos de autenticação/autorização. 
* A aplicação não precisa implementar os mecanismos de persistência em um banco de dados, eles podem ser persistidos "in memory" (ex.: Entity framework in memory).

## PONTOS QUE SERÃO AVALIADOS
* Arquitetura da aplicação - embora não existam muitos requisitos de negócio, iremos avaliar como a solution foi estruturada, bem como camadas e suas responsabilidades. 
* Programação orientada a objetos.
* Boas práticas como SOLID, TDD, DDD.
* Uso correto de injeção de dependências.
* Uso correto do padrão REST.
