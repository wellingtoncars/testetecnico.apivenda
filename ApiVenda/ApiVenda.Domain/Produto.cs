﻿namespace ApiVenda.Domain
{
    public class Produto
    {
        public int Id { get; set; }
        public int IdProduto { get; set; }
        public string NomeProduto { get; set; }
    }
}
