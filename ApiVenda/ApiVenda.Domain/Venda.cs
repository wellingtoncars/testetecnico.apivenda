﻿using System;

namespace ApiVenda.Domain
{
    public class Venda
    {
        public int Id { get; set; }
        public Vendedor Vendedor { get; set; }
        public StatusVenda StatusVenda { get; set; }
        public DateTimeOffset DataVenda { get; set; }
        public Pedido Pedido { get; set; }
    }

    public enum StatusVenda
    {
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        EnviadoTransportadora = 3,
        Entregue = 4,
        Cancelada = 5
    }
}
