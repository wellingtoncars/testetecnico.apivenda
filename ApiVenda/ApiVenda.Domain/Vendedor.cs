﻿namespace ApiVenda.Domain
{
    public class Vendedor
    {
        public int Id { get; set; }
        public long Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public long Telefone { get; set; }
        public int Ddd { get; set; }
    }
}
