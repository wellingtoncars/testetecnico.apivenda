﻿using System.Collections.Generic;

namespace ApiVenda.Domain
{
    public class Pedido
    {
        public int Id { get; set; }
        public IEnumerable<Produto> Produtos { get; set; }
    }
}
