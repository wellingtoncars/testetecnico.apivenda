﻿using System;

namespace ApiVenda.Domain.Exceptions
{
    public class CoreException : Exception
    {
        public CoreException(string message) : base(message)
        {
        }
    }
}
