﻿using System.Threading.Tasks;

namespace ApiVenda.Domain.Repository
{
    public interface IVendaRepository
    {
        /// <summary>
        /// Registra uma venda com o status "Aguardando pagamento".
        /// </summary>
        /// <param name="venda">Dados do vendedor e itens da venda.</param>
        /// <returns>Retorna o identificador da venda.</returns>
        Task<int> RegistraVendaAsync(Venda venda);

        /// <summary>
        /// Busca uma venda através do seu identificador.
        /// </summary>
        /// <param name="idVenda"></param>
        /// <returns>
        /// Retorna a venda correspondente 
        /// ao identificador pesquisado.
        /// </returns>
        Task<Venda> BuscaVendaAsync(int idVenda);

        /// <summary>
        /// Atualiza o status da venda.
        /// </summary>
        /// <param name="Venda">Dados da venda atualizada.</param>
        /// <returns></returns>
        Task AtualizaVendaAsync(Venda venda);
    }
}
