using ApiVenda.Application.Services;
using ApiVenda.Domain;
using ApiVenda.Domain.Exceptions;
using ApiVenda.Domain.Repository;
using ApiVenda.Domain.Services;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ApiVenda.Test
{
    public class VendaTest
    {
        private readonly IVendaService vendaService;
        private readonly Mock<IVendaRepository> vendaRepositoryMock;

        public VendaTest()
        {
            vendaRepositoryMock = new Mock<IVendaRepository>();
            vendaService = new VendaService(vendaRepositoryMock.Object);
        }

        [Fact]
        [Trait(nameof(IVendaService.RegistraVendaAsync), "Sucesso")]
        public async Task TesteRegistraVendaComSucessoAsync()
        {
            var venda = new Venda
            {
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.RegistraVendaAsync(It.IsAny<Venda>()))
                .ReturnsAsync(1);

            int idVendaEsperado = 1;

            var idVenda = await vendaService.RegistraVendaAsync(venda);

            Assert.Equal(idVendaEsperado, idVenda);
        }

        [Fact]
        [Trait(nameof(IVendaService.RegistraVendaAsync), "Erro")]
        public async Task TesteRegistraVendaProdutoNaoCadastradoAsync()
        {
            var venda = new Venda
            {
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido()
            };

            vendaRepositoryMock.Setup(v => v.RegistraVendaAsync(It.IsAny<Venda>()))
                .ReturnsAsync(1);

            string erroEsperado = "Gentileza informar o produto que est� sendo vendido.";

            var erroRetornado = await Assert.ThrowsAsync<CoreException>(async () =>
            {
                await vendaService.RegistraVendaAsync(venda);
            });

            Assert.Equal(erroEsperado, erroRetornado.Message);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Sucesso")]
        public async Task TesteAtualizaStatusVendaAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.PagamentoAprovado;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.AguardandoPagamento,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                .Returns(Task.CompletedTask).Callback((Venda vendaCallBack)=> 
                {
                    Assert.Equal(statusVenda, vendaCallBack.StatusVenda);
                });

            await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Erro")]
        public async Task TesteAtualizaStatusVendaParaStatusInvalidoAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.Entregue;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.AguardandoPagamento,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                .Returns(Task.CompletedTask);

            string erroEsperado = "A venda est� Aguardando pagamento e " +
                                 "n�o pode ser atualizada para o status solicitado.";

            var erroRetornado = await Assert.ThrowsAsync<CoreException>(async () =>
            {
                await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
            });

            Assert.Equal(erroEsperado, erroRetornado.Message);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Erro")]
        public async Task TesteAtualizaStatusVendaJaEntregueAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.Entregue;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.Entregue,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                .Returns(Task.CompletedTask);

            string erroEsperado = "Esta venda j� foi entregue.";

            var erroRetornado = await Assert.ThrowsAsync<CoreException>(async () =>
            {
                await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
            });

            Assert.Equal(erroEsperado, erroRetornado.Message);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Erro")]
        public async Task TesteAtualizaStatusVendaJaCanceladaAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.Entregue;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.Cancelada,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                .Returns(Task.CompletedTask);

            string erroEsperado = "Esta venda est� cancelada.";

            var erroRetornado = await Assert.ThrowsAsync<CoreException>(async () =>
            {
                await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
            });

            Assert.Equal(erroEsperado, erroRetornado.Message);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Erro")]
        public async Task TesteAtualizaStatusVendaPagamentoJaAprovadoAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.Entregue;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.PagamentoAprovado,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                .Returns(Task.CompletedTask);

            string erroEsperado = "A venda possui o pagamento aprovado e " +
                                "n�o pode ser atualizada para o status solicitado.";

            var erroRetornado = await Assert.ThrowsAsync<CoreException>(async () =>
            {
                await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
            });

            Assert.Equal(erroEsperado, erroRetornado.Message);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Sucesso")]
        public async Task TesteAtualizaStatusVendaPagamentoJaAprovadoSucessoAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.EnviadoTransportadora;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.PagamentoAprovado,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                .Returns(Task.CompletedTask).Callback((Venda vendaCallBack) =>
                {
                    Assert.Equal(statusVenda, vendaCallBack.StatusVenda);
                });

            await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Erro")]
        public async Task TesteAtualizaStatusVendaEnviadoATransportadoraAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.AguardandoPagamento;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.EnviadoTransportadora,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                .Returns(Task.CompletedTask);

            string erroEsperado = "N�o � poss�vel atualizar a venda para o status solicitado," +
                                 "pois ela j� se encontra com a transportadora.";

            var erroRetornado = await Assert.ThrowsAsync<CoreException>(async () =>
            {
                await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
            });

            Assert.Equal(erroEsperado, erroRetornado.Message);
        }

        [Fact]
        [Trait(nameof(IVendaService.AtualizaVendaAsync), "Sucesso")]
        public async Task TesteAtualizaStatusVendaEnviadoATransportadoraSucessoAsync()
        {
            int idVenda = 1;
            var statusVenda = StatusVenda.Entregue;

            var venda = new Venda
            {
                StatusVenda = StatusVenda.EnviadoTransportadora,
                Vendedor = new Vendedor
                {
                    Cpf = 55478854478,
                    Email = "fulano@gmail.com",
                    Ddd = 31,
                    Telefone = 9788546589,
                    Nome = "Fulano"
                },
                Pedido = new Pedido
                {
                    Produtos = new List<Produto>
                    {
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Mouse"
                        },
                        new Produto
                        {
                            IdProduto = 5578,
                            NomeProduto = "Teclado"
                        }
                    }
                }
            };

            vendaRepositoryMock.Setup(v => v.BuscaVendaAsync(It.IsAny<int>()))
                .ReturnsAsync(venda);

            vendaRepositoryMock.Setup(v => v.AtualizaVendaAsync(It.IsAny<Venda>()))
                 .Returns(Task.CompletedTask).Callback((Venda vendaCallBack) =>
                 {
                     Assert.Equal(statusVenda, vendaCallBack.StatusVenda);
                 });

            await vendaService.AtualizaVendaAsync(idVenda, statusVenda);
        }
    }
}
