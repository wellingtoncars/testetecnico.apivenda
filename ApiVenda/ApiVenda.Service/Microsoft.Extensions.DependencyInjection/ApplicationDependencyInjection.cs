﻿using ApiVenda.Application.Services;
using ApiVenda.Domain.Services;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ApplicationDependencyInjection
    {
        public static IServiceCollection AddApplication
            (this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddScoped<IVendaService, VendaService>();

            return services;
        }
            
    }
}
