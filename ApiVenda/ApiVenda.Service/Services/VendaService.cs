﻿using ApiVenda.Domain;
using ApiVenda.Domain.Exceptions;
using ApiVenda.Domain.Repository;
using ApiVenda.Domain.Services;
using ApiVenda.Validator;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ApiVenda.Application.Services
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepository vendaRepository;

        public VendaService(IVendaRepository vendaRepository)
        {
            this.vendaRepository = vendaRepository 
                ?? throw new ArgumentNullException(nameof(vendaRepository));
        }

        public async Task AtualizaVendaAsync(int idVenda, StatusVenda statusVenda)
        {
            var venda = await vendaRepository.BuscaVendaAsync(idVenda);

            switch (venda.StatusVenda)
            {
                case StatusVenda.Cancelada:
                    throw new CoreException("Esta venda está cancelada.");

                case StatusVenda.Entregue:
                    throw new CoreException("Esta venda já foi entregue.");

                case StatusVenda.AguardandoPagamento:
                    if (statusVenda != StatusVenda.Cancelada && statusVenda != StatusVenda.PagamentoAprovado)
                    {
                        throw new CoreException("A venda está Aguardando pagamento e " +
                                 "não pode ser atualizada para o status solicitado.");
                    }
                    break;

                case StatusVenda.PagamentoAprovado:
                    if (statusVenda != StatusVenda.EnviadoTransportadora && statusVenda != StatusVenda.Cancelada)
                    {
                        throw new CoreException("A venda possui o pagamento aprovado e " +
                                "não pode ser atualizada para o status solicitado.");
                    }
                    break;

                case StatusVenda.EnviadoTransportadora:
                    if (statusVenda != StatusVenda.Entregue)
                    {
                        throw new CoreException("Não é possível atualizar a venda para o status solicitado," +
                                 "pois ela já se encontra com a transportadora.");
                    }
                    break;
            }

            venda.StatusVenda = statusVenda;

            await vendaRepository.AtualizaVendaAsync(venda);
        }

        public async Task<Venda> BuscaVendaAsync(int idVenda)
        {
            return await vendaRepository.BuscaVendaAsync(idVenda);
        }

        public async Task<int> RegistraVendaAsync(Venda venda)
        {
            VendedorValidator validator = new VendedorValidator();

            var validacao = validator.Validate(venda.Vendedor);

            if (!validacao.IsValid)
            {
                throw new CoreException(validacao.Errors.FirstOrDefault().ErrorMessage);
            }
            if (venda.Pedido.Produtos == null || !venda.Pedido.Produtos.Any())
            {
                throw new CoreException("Gentileza informar o produto que está sendo vendido.");
            }
            venda.StatusVenda = StatusVenda.AguardandoPagamento;
            venda.DataVenda = DateTimeOffset.Now;

            return await vendaRepository.RegistraVendaAsync(venda);
        }
    }
}
