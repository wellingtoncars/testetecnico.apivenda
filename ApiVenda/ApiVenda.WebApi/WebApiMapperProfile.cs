﻿using ApiVenda.Domain;
using ApiVenda.WebApi.Dto;
using AutoMapper;

namespace ApiVenda.WebApi
{
    public class WebApiMapperProfile : Profile
    {
        public WebApiMapperProfile()
        {
            CreateMap<VendaPost, Venda>()
                .ForMember(v=> v.Pedido, opt=> opt.MapFrom(vdto=> new Pedido()));

            CreateMap<VendedorDto, Vendedor>();
            CreateMap<ProdutoDto, Produto>();
            CreateMap<ProdutoDto, Pedido>();

            CreateMap<Venda, VendaGetResult>();
            CreateMap<Vendedor, VendedorDto>();
            CreateMap<Pedido, PedidoDto>();
            CreateMap<Produto, ProdutoDto>();
        }
    }
}
