﻿using System.Collections.Generic;

namespace ApiVenda.WebApi.Dto
{
    public class VendaPost
    {
        public VendedorDto Vendedor { get; set; }

        public IEnumerable<ProdutoDto> Produtos { get; set; }
    }
}
