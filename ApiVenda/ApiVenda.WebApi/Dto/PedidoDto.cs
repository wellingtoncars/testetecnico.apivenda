﻿using System.Collections.Generic;

namespace ApiVenda.WebApi.Dto
{
    public class PedidoDto
    {
        public IEnumerable<ProdutoDto> Produtos { get; set; }
    }
}
