﻿namespace ApiVenda.WebApi.Dto
{
    public class ProdutoDto
    {
        public int IdProduto { get; set; }
        public string NomeProduto { get; set; }
    }
}
