﻿namespace ApiVenda.WebApi.Dto
{
    public class VendedorDto
    {
        public long Cpf { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public long Ddd { get; set; }

        public long Telefone { get; set; }
    }
}
