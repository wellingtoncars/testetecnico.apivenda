﻿using System;

namespace ApiVenda.WebApi.Dto
{
    public class VendaGetResult
    {
        public VendedorDto Vendedor { get; set; }
        public PedidoDto Pedido { get; set; }
        public StatusVendaDto StatusVenda { get; set; }
        public DateTimeOffset DataVenda { get; set; }
    }

    public enum StatusVendaDto
    {
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        EnviadoTransportadora = 3,
        Entregue = 4,
        Cancelada = 5
    }
}
