﻿using ApiVenda.Domain;
using ApiVenda.Domain.Exceptions;
using ApiVenda.Domain.Services;
using ApiVenda.WebApi.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiVenda.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly IVendaService vendaService;
        private readonly IMapper mapper;

        public VendasController(IVendaService vendaService, IMapper mapper)
        {
            this.vendaService = vendaService
                ?? throw new ArgumentNullException(nameof(vendaService));

            this.mapper = mapper
                ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpPost]
        [ProducesResponseType(typeof(int), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> RegistraVendaAsync(VendaPost vendaDto)
        {
            var venda = mapper.Map<Venda>(vendaDto);

            venda.Pedido.Produtos = mapper.Map<IEnumerable<Produto>>(vendaDto.Produtos);

            int idVenda;

            try
            {
                idVenda = await vendaService.RegistraVendaAsync(venda);
            }
            catch (CoreException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(idVenda);
        }

        [HttpGet("{Id}")]
        [ProducesResponseType(typeof(VendaGetResult), 200)]
        [ProducesResponseType(typeof(NoContentResult), 204)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> BuscaVendaAsync([FromRoute(Name = "Id")]int id)
        {
            var venda = await vendaService.BuscaVendaAsync(id);

            if(venda == null)
            {
                return NoContent();
            }
            var vendaGetresult = mapper.Map<VendaGetResult>(venda);

            return Ok(vendaGetresult);
        }

        [HttpPatch("{Id}")]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> AtualizaVendaAsync([FromRoute(Name = "Id")]int id,
            StatusVendaDto statusVenda)
        {
            try
            {
                await vendaService.AtualizaVendaAsync(id, (StatusVenda)statusVenda);
            }
            catch (CoreException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }
    }
}