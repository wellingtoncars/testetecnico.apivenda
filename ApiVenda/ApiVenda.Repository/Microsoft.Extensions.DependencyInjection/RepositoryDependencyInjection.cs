﻿using ApiVenda.Domain.Repository;
using ApiVenda.Repository;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class RepositoryDependencyInjection
    {
        public static IServiceCollection AddRepository
            (this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddScoped<IVendaRepository, VendaRepository>();

            return services;
        }
    }
}
