﻿using ApiVenda.Domain;
using ApiVenda.Domain.Exceptions;
using ApiVenda.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ApiVenda.Repository
{
    public class VendaRepository : IVendaRepository
    {
        private readonly RepositoryContext context;

        public VendaRepository(RepositoryContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task AtualizaVendaAsync(Venda venda)
        {
            var vendaAtual = context.Vendas.SingleOrDefault(b => b.Id == venda.Id);

            if (vendaAtual == null)
            {
                throw new CoreException("Venda não encontrada.");
            }
            vendaAtual.StatusVenda = venda.StatusVenda;
            await context.SaveChangesAsync();
        }

        public async Task<Venda> BuscaVendaAsync(int idVenda)
        {
            var venda = await Task.FromResult(
                context.Vendas
                .Include(p=> p.Pedido)
                .Include(p=> p.Pedido.Produtos)
                .Include(p=> p.Vendedor)
                .Where(v => v.Id == idVenda).FirstOrDefault());
            return venda;
        }

        public async Task<int> RegistraVendaAsync(Venda venda)
        {
            context.Vendas.Add(venda);
            await context.SaveChangesAsync();

            return venda.Id;
        }
    }
}
