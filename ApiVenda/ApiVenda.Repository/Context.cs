﻿using ApiVenda.Domain;
using Microsoft.EntityFrameworkCore;

namespace ApiVenda.Repository
{
    public class RepositoryContext: DbContext
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options)
         : base(options)
        { }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedors { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }
    }
}
