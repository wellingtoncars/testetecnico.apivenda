﻿using ApiVenda.Domain;
using FluentValidation;

namespace ApiVenda.Validator
{
    public class VendedorValidator : AbstractValidator<Vendedor>
    {
        public VendedorValidator()
        {
            RuleFor(x => x.Cpf).NotEmpty().WithMessage("O cpf do vendedor deve ser informado.");
            RuleFor(x => x.Telefone).NotEmpty().WithMessage("O telefone do vendedor deve ser informado.");
            RuleFor(x => x.Email).EmailAddress().WithMessage("Informe um e-mail válido para o vendedor.");
            RuleFor(x => x.Nome).NotEmpty().WithMessage("Informe o nome do vendedor.");
        }
    }
}
